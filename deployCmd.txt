docker run -p5901:5900 `
--net=host `
-v ~/.kube:/root/.kube `
--rm -it --name kubedoom `
storaxdev/kubedoom:0.5.0

kubectl create namespace vote
kubectl create -f k8s-specifications/ -n vote